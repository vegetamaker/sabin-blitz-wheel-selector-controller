;==================================================================================
;## Go to FF6
;==================================================================================
	IfWinExist, Final Fantasy VI	; checks to see if FF6 is open
		WinActivate Final Fantasy VI	; Brings FF6 window to the front and resets number keys to run blitz combos.

	
;==================================================================================
;## Global Variables
;==================================================================================
	Path = A_ScriptDir
	
	SetTimer, WatchAxis
	
	;Gui, Add, Picture, x0 y0 w500 h233 vGMyPic, %Path%\..\SabinWheel.png
	if FileExist("%Path%\..\settings.ini")
		{
		IniRead, saved_status, settings.ini, status, saved_status
		status = %saved_status%
		
		IniRead, saved_skill, settings.ini, skill, saved_skill
		skillSelected = %saved_skill%
		
		IniRead, saved_skillname, settings.ini, skill, saved_skillname
		skillName = %saved_skillname%
		}
	else
		{
		status = 1
		skillSelected = 4
		skillName = Rising Phoenix
		}
	
;==================================================================================
;## Window
;==================================================================================
	
	IniRead, gui_position, settings.ini, window position, gui_position, Center	
	
	Gui, Color, Black
	Gui, +AlwaysOnTop
	Gui, +Hwndgui_id
	Gui, Show, %gui_position% w500 h333 NA, Sabin Blitzs!
	AutoTrim, off
	

	Ins1 = L2 -> Change Wheel
	Ins2 = Right Stick to Change Skill
	Ins3 = R2 -> Execute skill
	
	
	Gui, font, s12 bold, Times New Roman
	Gui, add, Text, X10 y240 cYellow vGIns1, %Ins1%
	GuiControl +BackgroundTrans, GIns1
	
	Gui, font, s12 bold, Times New Roman
	Gui, add, Text, X10 y270 cYellow vGIns2, %Ins2%
	GuiControl +BackgroundTrans, GIns2
	
	Gui, font, s12 bold, Times New Roman
	Gui, add, Text, X10 y300 cYellow vGIns3, %Ins3%
	GuiControl +BackgroundTrans, GIns3
	
	
	
	Gui, font, s12 bold, Times New Roman
	Gui, add, Text, X350 y240 cYellow vGWheel, WHEEL:
	GuiControl +BackgroundTrans, GWheel
	
	Gui, font, s20 bold, Times New Roman
	Gui, add, Text, X420 y233 cRed vGStatus, %status%
	GuiControl +BackgroundTrans, GStatus	
	
		
	Gui, font, s12 bold, Times New Roman
	Gui, add, Text, X355 y280 cYellow vGSkill, SKILL:
	GuiControl +BackgroundTrans, GSkill
	
	Gui, font, s20 bold, Times New Roman
	Gui, add, Text, X280 y300 w200 +Center cRed vGSName, %skillName%
	GuiControl +BackgroundTrans, GSName
	
	
	
	;Path = A_ScriptDir ;No needed anymore, used before.
	Gui, Add, Picture, x0 y0 w500 h233 vGMyPic, %Path%\..\SabinWheel.png
	GuiControl +BackgroundTrans, GMyPic
		
	
	
;==================================================================================
;## L2 - Change "Wheel menu". 1 (skills 1-4). 2 (Skills 5-8).
;==================================================================================
	Joy7::
		{
		if (status = 1)
			{
			status = 2
			}
		else
			{
			status = 1
			}
		GuiControl,, GStatus, %status%
		GuiControl +BackgroundTrans, GStatus
		return			
		}
		
;==================================================================================
;## Change Skill Selected Function
;==================================================================================		
	WatchAxis:
		{			
			JoyX := GetKeyState("JoyZ")  ; Get position of X axis.
			JoyY := GetKeyState("JoyR")  ; Get position of Y axis.
				
		if(status = 1)
			{
			if (JoyX > 70)
				{
				skillSelected = 3 ; Send "RIGHT"
				skillName = Meteor Strike
				}
			else if (JoyX < 20)
				{
				skillSelected = 1 ; Send "LEFT"
				skillName = Raging Fist
				}
			else if (JoyY > 70)
				{
				skillSelected = 2 ; Send "DOWN"
				skillName = Aura Canon
				}
			else if (JoyY < 20)
				{
				skillSelected = 4 ; Send "UP"
				skillName = Rising Phoenix
				}
			}
		else
			{
			if (JoyX > 70)
				{
				skillSelected = 7 ; Send "RIGHT"
				skillName = Soul Spiral
				}
			else if (JoyX < 20)
				{
				skillSelected = 5 ; Send "LEFT"
				skillName = Chakra
				}
			else if (JoyY > 70)
				{
				skillSelected = 6 ; Send "DOWN"
				skillName = Razor Gale
				}
			else if (JoyY < 20)
				{
				skillSelected = 8 ; Send "UP"
				skillName = Phantom Rush
				}
			}
		
		GuiControl,, GSName, %skillName%
		GuiControl +BackgroundTrans, GSName
		return
		}
	
;==================================================================================
;## R2 - Execute Skill Selected
;==================================================================================	
	Joy8::
		{
		Switch skillSelected
			{
			Case 1: ; Raging Fist
				Number1()
				return
			Case 2: ; Aura Canon
				Number2()
				return
			Case 3: ; Meteor Strike
				Number3()
				return
			Case 4: ; Rising Phoenix
				Number4()
				return
			Case 5: ; Chakra
				Number5()
				return
			Case 6: ; Razor Gale
				Number6()
				return
			Case 7: ; Soul Spiral
				Number7()
				return
			Case 8: ; Phantom Rush
				Number8()
				return
			Default:
				Number1()
				return
			}	
		}


;==================================================================================
;## 1 - Raging Fist
;==================================================================================
Number1(){
		Send {Left down}
		Sleep 100
		Send {Left up}
		Sleep 100
		Send {Right down}
		Sleep 100
		Send {Right up}
		Sleep 100
		Send {Left down}
		Sleep 100
		Send {Left up}
		Sleep 100
		Send {Enter down}
		Sleep 100
		Send {Enter up}
		return
	}

;==================================================================================
;## 2 - Aura Cannon
;==================================================================================
Number2(){
		Send {Down down}
		Sleep 100
		Send {Left down}
		Sleep 100
		Send {Down up}
		Sleep 100
		Send {Left up}
		Sleep 100
		Send {Enter down}
		Sleep 100
		Send {Enter up}
		return
	}

;==================================================================================
;## 3 - Meteor Strike
;==================================================================================
Number3(){
		Send {Right down}
		Send {Up down}
		Sleep 100
		Send {Right up}
		Send {Up up}
		Sleep 100
		Send {Left down}
		Send {Up down}
		Sleep 100
		Send {Left up}
		Send {Up up}
		Sleep 100
		Send {Down down}
		Sleep 100
		Send {Down up}
		Sleep 100
		Send {Up down}
		Sleep 100
		Send {Up up}
		Sleep 100
		Send {Enter down}
		Sleep 100
		Send {Enter up}
		return
	}
	
;==================================================================================
;## 4 - Rising Phoenix
;==================================================================================
Number4(){
		Send {Left down}
		Sleep 100
		Send {Down down}
		Sleep 100
		Send {Left up}
		Sleep 100
		Send {Right down}
		Sleep 100
		Send {Down up}
		Sleep 100
		Send {Right up}
		Sleep 100
		Send {Enter down}
		Sleep 10
		Send {Enter up}
		return
	}
	
;==================================================================================
;## 5 - Chakra
;==================================================================================
Number5(){
		Send {Up down}
		Send {Right down}
		Sleep 100
		Send {Up up}
		Send {Right up}
		Sleep 100
		Send {Left down}
		Send {Up down}
		Sleep 100
		Send {Left up}
		Send {Up up}
		Sleep 100
		Send {Right down}
		Send {Down down}
		Sleep 100
		Send {Down up}
		Send {Right up}
		Sleep 100
		Send {Left down}
		Send {Down down}
		Sleep 100
		Send {Left up}
		Send {Down up}
		Sleep 100
		Send {Down down}
		Sleep 100
		Send {Down up}
		Sleep 100
		Send {Up down}
		Sleep 100
		Send {Up up}
		Sleep 100
		Send {Enter down}
		Sleep 100
		Send {Enter up}
		return
	}
	
;==================================================================================
;## 6 - Razor Gale
;==================================================================================
Number6(){
		Send {Up down}
		Sleep 100
		Send {Right down}
		Sleep 100
		Send {Up up}
		Sleep 100
		Send {Down down}
		Sleep 100
		Send {Right up}
		Sleep 100
		Send {Left down}
		Sleep 100
		Send {Down up}
		Sleep 100
		Send {Left up}
		Sleep 100
		Send {Enter down}
		Sleep 100
		Send {Enter up}
		return
	}
	
;==================================================================================
;## 7 - Soul Spiral
;==================================================================================
Number7(){
		Send {Right down}
		Send {Up down}
		Sleep 100
		Send {Up up}
		Send {Right up}
		Sleep 100
		Send {Left down}
		Send {Up down}
		Sleep 100
		Send {Left up}
		Send {Up up}
		Sleep 100
		Send {Up down}
		Sleep 100
		Send {Up up}
		Sleep 100
		Send {Down down}
		Sleep 100
		Send {Down up}
		Sleep 100
		Send {Right down}
		Sleep 100
		Send {Right up}
		Sleep 100
		Send {Left down}
		Sleep 100
		Send {Left up}
		Sleep 100
		Send {Enter down}
		Sleep 100
		Send {Enter up}		
		return
	}
	
;==================================================================================
;## 8 - Phantom Rush
;==================================================================================
Number8(){
		Send {Left down}
		Sleep 100
		Send {Up down}
		Sleep 100
		Send {Left up}
		Sleep 100
		Send {Right down}
		Sleep 100
		Send {Up up}
		Sleep 100
		Send {Down down}
		Sleep 100
		Send {Right up}
		Sleep 100
		Send {Left down}
		Sleep 100
		Send {Down up}
		Sleep 100
		Send {Left up}
		Sleep 100
		Send {Enter down}
		Sleep 100
		Send {Enter up}		
		return
	}
	
;==================================================================================
;## Window Close
;==================================================================================
	GuiClose:
		{
		WinGetPos, gui_x, gui_y,,, ahk_id %gui_id%
		IniWrite, x%gui_x% y%gui_y%, settings.ini, window position, gui_position
		
		IniWrite, %status%, settings.ini, status, saved_status
		IniWrite, %skillSelected%, settings.ini, skill, saved_skill
		IniWrite, %skillName%, settings.ini, skill, saved_skillname
		
		ExitApp
		}